package light_in.morpher;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Morpher {
    private static final Map<Integer, String> numberNames = new HashMap<>();

    static {
        String filePath = "map.txt";
        String line = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (reader != null) {
                    line = reader.readLine();
                    if (line == null)
                        break;
                }
            } catch (IOException | NullPointerException e) {
                e.printStackTrace();
            }
            String[] parts = line.split(":", 2);
            if (parts.length >= 2) {
                try {
                    int key = Integer.parseInt(parts[0]);
                    String value = parts[1];
                    numberNames.put(key, value);
                } catch (final NumberFormatException e) {
                    System.out.println("ignoring line: " + line);
                }
            } else {
                System.out.println("ignoring line: " + line);
            }
        }
       /*numberNames.put(1, " один");
        numberNames.put(2, " два");
        numberNames.put(3, " три");
        numberNames.put(4, " четыре");
        numberNames.put(5, " пять");
        numberNames.put(6, " шесть");
        numberNames.put(7, " семь");
        numberNames.put(8, " восемь");
        numberNames.put(9, " девять");
        numberNames.put(10, "десять");
        numberNames.put(11, "одиннадцать");
        numberNames.put(12, "двенадцать");
        numberNames.put(13, "тринадцать");
        numberNames.put(14, "четырнадцать");
        numberNames.put(15, "пятнадцать");
        numberNames.put(16, "шестнадцать");
        numberNames.put(17, "семнадцать");
        numberNames.put(18, "восемнадцать");
        numberNames.put(19, "девятнадцать");
        numberNames.put(20, "двадцать");
        numberNames.put(30, "тридцать");
        numberNames.put(40, "сорок");
        numberNames.put(50, "пятьдесят");
        numberNames.put(60, "шестьдесят");
        numberNames.put(70, "семьдесят");
        numberNames.put(80, "восемьдесят");
        numberNames.put(90, "девяносто");*/
    }

    public static String morph(int numToMorph) throws InvNumberException {
        String morphedString = "";
        if (numToMorph > 99 || numToMorph < 1) {
            throw new InvNumberException("Wrong number!");
        }
        if (numToMorph > 9 && numToMorph < 20) {
            morphedString += numberNames.get(numToMorph);
        } else {
            if (numberNames.containsKey((numToMorph / 10) * 10))
                morphedString += numberNames.get((numToMorph / 10) * 10);
            if (numberNames.containsKey(numToMorph % 10))
                morphedString += numberNames.get(numToMorph % 10);
        }
        return morphedString.trim();
    }
}
package light_in.morpher;

public class InvNumberException extends Throwable {
    public InvNumberException(String message) {
        super (message);
    }
}
package light_in.morpher;
import org.junit.jupiter.api.Assertions;

import java.util.Scanner;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Main {

    public static void main(String[] args) throws InvNumberException {
        assertEquals(Morpher.morph(17),"семнадцать");
        assertEquals(Morpher.morph(1),"один");
        assertEquals(Morpher.morph(97),"девяносто семь");
        assertEquals(Morpher.morph(50),"пятьдесят");
        assertEquals(Morpher.morph(99),"девяносто девять");
        assertEquals(Morpher.morph(7),"семь");
        InvNumberException thrown = Assertions.assertThrows(InvNumberException.class, () -> Morpher.morph(-20));
        Assertions.assertEquals("Wrong number!", thrown.getMessage());
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число для преобразования в текст (от 1 до 99): ");
        int numToMorph = in.nextInt();
        in.close();
        String morphedStrOrError = Morpher.morph(numToMorph);
        System.out.print("Ваше число буквами: \n");
        System.out.print(morphedStrOrError);
    }
}
